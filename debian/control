Source: ratpoints
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libgmp-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/ratpoints
Vcs-Git: https://salsa.debian.org/science-team/ratpoints.git
Homepage: https://www.mathe2.uni-bayreuth.de/stoll/programs/
Rules-Requires-Root: no

Package: ratpoints
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: find rational points on hyperelliptic curves
 This program tries to find all rational points within a given height
 bound on a hyperelliptic curve in a very efficient way, by using
 an optimized quadratic sieve algorithm.

Package: libratpoints-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         libratpoints-2.2.1 (= ${binary:Version})
Multi-Arch: same
Description: development files for libratpoints
 This program tries to find all rational points within a given height
 bound on a hyperelliptic curve in a very efficient way, by using
 an optimized quadratic sieve algorithm.
 .
 This package contains the development files for the library.

Package: libratpoints-2.2.1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: library for finding rational points on hyperelliptic curves
 This program tries to find all rational points within a given height
 bound on a hyperelliptic curve in a very efficient way, by using
 an optimized quadratic sieve algorithm.
 .
 This package contains the shared library.
